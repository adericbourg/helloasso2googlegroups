<?php
declare(strict_types=1);

require_once 'vendor/autoload.php';
include "google_client.php";

class GoogleGroupClient {
    var $groupEmail;
    var $service;

    function __construct($email)
    {
        $this->groupEmail= $email;
    }

    function connect() {
        $client = getClient();
        $this->service = new Google_Service_Directory($client);
    }

    function register_all($data) {
        foreach ($data as $user_info) {
            $this->register_member($user_info);
        }
    }

    function register_member($user_info) {
        $member = new Google_Service_Directory_Member();
        $member->setEmail($user_info["email"]);
        $member->setRole("MEMBER");
        $this->service->members->insert( $this->groupEmail, $member);
    }
}
